class CreateZombies < ActiveRecord::Migration
  def change
    create_table :zombies do |t|
      t.string :username
      t.string :password

      t.timestamps
    end
  end
end
