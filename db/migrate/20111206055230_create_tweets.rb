class CreateTweets < ActiveRecord::Migration
  def change
    create_table :tweets do |t|
      t.text :tweet
      t.integer :zombie_id

      t.timestamps
    end
  end
end
