class Zombie < ActiveRecord::Base
  has_many :tweets
  
  validates :username, :presence => true, :length => {:minimum=>2, :maximum=>20}, :uniqueness =>true
  validates :password, :presence => true
end
