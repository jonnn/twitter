class Tweet < ActiveRecord::Base
  belongs_to :zombie
  
  validates :zombie_id, :presence => true
  validates :tweet, :presence => true, :length =>{:maximum=>155, :minimum=>2}
end
