class StatController < ApplicationController
  def index
    @zombie_last = Zombie.last.username
    @zombie_count = Zombie.count
    @tweet_count = Tweet.count
    @tweet_last = Tweet.last.tweet
    @title = "Stats"
  end

end
